<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse justify-content-between" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link @if(Route::current()->getName() === 'default')active @endif" href="{{ route('default') }}">Home</a>
      <a class="nav-item nav-link @if(Route::current()->getName() === 'products.all')active @endif" href="{{ route('products.all') }}">Products</a>
    </div>
    <div class="navbar-nav">
        @guest
            <a class="nav-item nav-link @if(Route::current()->getName() === 'login')active @endif" href="{{ route('login') }}">Login</a>
            <a class="nav-item nav-link @if(Route::current()->getName() === 'register')active @endif" href="{{ route('register') }}">Register</a>
            <a class="nav-item nav-link" href="{{ route('google.login') }}">Google Sign in</a>
        @else
            <h6 class="welcome"> Welcome, {{ Auth::user()->name }}!</h6>
            <a class="nav-item nav-link" href="{{ route('logout') }}">Logout</a>
        @endguest
      <h4 class="mr-auto text-danger laravel"> {{ env('APP_NAME') }}</h4>
    </div>
  </div>
</nav>