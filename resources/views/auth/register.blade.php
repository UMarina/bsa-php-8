@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="form" method="POST" action="{{ route('register') }}">
            <h2 class="signin-title">Register Form</h2>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" placeholder="Enter email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Enter password" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation" required>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-lg btn-block signin-btn">Register</button>
            </div>

            {{ csrf_field() }}
        </form>
    </div>
@endsection