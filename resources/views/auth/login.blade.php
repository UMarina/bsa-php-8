@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="form" action="{{ route('login') }}" method="post">
            <h2 class="signin-title">Enter credentials to login</h2>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="login-email" class="form-control" name="email" type="email" placeholder="Enter email" value="{{ old('email') }}" required="" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="login-password" class="form-control" name="password" type="password" placeholder="Enter password" required="">
                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>
            <div class="text-center">
                <button class="btn btn-lg btn-block signin-btn" type="submit">Login</button>
            </div>

            {{ csrf_field() }}
        </form>
    </div>
@endsection