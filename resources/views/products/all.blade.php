@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="products-header">List of products <a class="right-link" title="Add product" href="{{ route('product.add')}}"><i class="fas fa-plus"></i> Add</a></h3>
    <ul class="category-list">
        @if($products)
            @foreach($products as $product)
                <li>
                    <a class="product-link" href="{{ route('product.show',$product->id) }}">{{ $product->name }}</a>
                    <div class="right-block">
                        <span>  {{ $product->price }}</span>
                    </div>
                </li>
            @endforeach
        @endif
    </ul>
</div>
@endsection