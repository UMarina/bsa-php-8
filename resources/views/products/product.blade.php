@extends('layouts.app')

@section('content')
    <div class="container">
        <h3 class="products-header">
            <a class="left-link" title="Return" href="{{ route('products.all')}}"><i class="fas fa-arrow-circle-left"></i></a>
            Product
        </h3>
        <ul class="category-list">
            @if($product)
                <li>{{ $product->name }}
                    <div class="right-block">
                        <span>  {{ $product->price }}</span>
                        @can('update',$product)
                        <a title="Edit product" href="{{ route('product.edit', $product->id)}}"><i class="far fa-edit"> Edit</i></a>
                        @endcan
                        @can('delete',$product)
                        <form method="post" action="{{route('product.delete',$product->id)}}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE')}}
                            <button title="Delete product" type="submit"><i class="far fa-trash-alt"></i> Delete</button>
                        </form>
                        @endcan
                    </div>
                </li>
            @endif
        </ul>
    </div>
@endsection