@extends('layouts.app')

@section('content')
    <form class="form" action="{{ route('product.store') }}" method="post">
        <h2 class="text-center">Add product</h2>
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <input class="form-control" name="name" type="text" placeholder="Enter product name" value="" required="">
            @if ($errors->has('name'))
                <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
            <input class="form-control" name="price" type="text" placeholder="Enter product price" value="" required="">
            @if ($errors->has('price'))
                <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
            @endif
        </div>
        <div class="text-center">
            <button class="btn btn-lg btn-block signin-btn" type="submit">Save</button>
        </div>

        {{ csrf_field() }}
    </form>
@endsection