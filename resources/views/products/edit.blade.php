@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="form" action="{{ route('product.update', $product->id) }}" method="post">
            <h3 class="products-header">
                <a class="left-link" title="Return to product list" href="{{ route('products.all')}}"><i class="fas fa-arrow-circle-left"></i></a>
                Edit product
            </h3>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input class="form-control" name="name" type="text" placeholder="Enter product name" value="{{ $product->name }}" required="">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                <input class="form-control" name="price" type="text" placeholder="Enter product price" value="{{ $product->price }}" required="">
                @if ($errors->has('price'))
                    <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
            <div class="text-center">
                <button class="btn btn-lg btn-block signin-btn" type="submit">Save</button>
            </div>

            {{ csrf_field() }}
            {{ method_field('PUT')}}
        </form>
    </div>
@endsection