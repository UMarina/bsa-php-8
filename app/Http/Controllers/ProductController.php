<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity\Product;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view ('products.all',[
            'products' => $products
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->except(['_token']);
        $request->validate([
            'name' => 'required|max:255',
            'price'=> 'required'
        ]);

        $data['user_id'] = auth()->id();
        Product::create($data);
        return redirect()->route('products.all');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if ($product) {
            return view ('products.product',[
                'product' => $product
            ]);
        } else {
            return redirect()->route('products.all');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        if ($product) {
            if (Gate::denies('update', $product)) {
                return redirect()->route('products.all');
            }
           // $this->authorize('update', $product);
            return view('products.edit', [
                'product' => $product
            ]);
        }
        return redirect()->route('products.all');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if ($product) {
            if (Gate::denies('update', $product)) {
                return redirect()->route('products.all');
            }
            //$this->authorize('update', $product);
            $data=$request->except(['_token']);
            $request->validate([
                'name' => 'required|max:255',
                'price'=> 'required'
            ]);

            $product->update($data);
            return redirect()->route('product.show',[
                'id'=>$product->id
            ]);
        }
        return redirect(route('products.all'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product) {
            if (Gate::denies('delete', $product)) {
                return redirect()->route('products.all');
            }
            Product::destroy($id);
        }
        return redirect(route('products.all'));
    }
}
