<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use App\Entity\User;
use Illuminate\Support\Facades\Hash;

class SocialAuthController extends Controller
{
    protected $redirectTo = '/products';

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }


    public function handleProviderCallback()
    {
        $userData = Socialite::driver('google')->user();

        $user = $this->findUserOrCreate($userData);

        Auth::login($user);

        return redirect($this->redirectTo);



    }
    public function findUserOrCreate($userData)
    {
        if ($user = $this->findUserByGoogleId($userData->getId())) {
            return $user;
        }
        if ($user = $this->findUserByEmail($userData->getEmail())) {
            return $user;
        }

        $user = User::create([
            'name' => $userData->getName(),
            'email' => $userData->getEmail(),
            'google_id' => $userData->getId(),
            'password' => Hash::make(str_random(12))
        ]);

        return $user;
    }

    public function findUserByGoogleId($id)
    {
        $socialAccount = User::where('google_id', $id)->first();

        return $socialAccount ? $socialAccount : false;
    }

    public function findUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }
}