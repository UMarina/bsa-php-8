<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@index')->name('default');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();

Route::get('login/google', 'Auth\SocialAuthController@redirectToProvider')->name('google.login');
Route::get('login/google/callback', 'Auth\SocialAuthController@handleProviderCallback');

Route::group(['prefix' => '/products'], function () {
    Route::get('/', 'ProductController@index')->name('products.all');
    Route::get('/add', 'ProductController@create')->name('product.add');
    Route::post('/', 'ProductController@store')->name('product.store');
    Route::get('/{id}', 'ProductController@show')->name('product.show');
    Route::get('/{id}/edit', 'ProductController@edit')->name('product.edit');
    Route::put('/{id}', 'ProductController@update')->name('product.update');
    Route::delete('/{id}', 'ProductController@destroy')->name('product.delete');
});